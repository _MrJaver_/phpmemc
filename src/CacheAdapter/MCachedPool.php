<?php
/**
 * Created by PhpStorm.
 * User: return0
 * Date: 5/24/2018
 * Time: 3:40 PM
 */

namespace CacheAdapter;

use Cache\Adapter\Common\PhpCacheItem;
use Cache\Adapter\Common\TagSupportWithArray;
use Cache\Hierarchy\HierarchicalCachePoolTrait;
use Memcached;

class MCachedPool extends AbstractMCachePool
{

    use HierarchicalCachePoolTrait;
    use TagSupportWithArray;

    /**
     * @type \Memcached
     */
    protected $cache;

    /**
     * @param \Memcached $cache
     */
    public function __construct(Memcached $cache)
    {
        $this->cache = $cache;
        $this->cache->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);
    }

    protected function init()
    {
        if($this->cache == null){
            $this->cache = new Memcached();
            $this->cache->addServer('localhost', 11211);
        }
    }

    /**
     * Returns all the keys stored in cache to the client
     *
     * @return array
     *  Array with all the keys
     */
    public function getKeys()
    {
        return $this->cache->getAllKeys();
    }

    /**
     * Returns all items stored in cache server to the client
     *
     * @return array of CacheItem
     *  Array with all items from cache
     */
    public function getAllItems(){
        $items = array();

        $keys = $this->getKeys();
        foreach ($keys as $key){
            $items[] = $this->getItem($key);
        }

        return $items;
    }

    /**
     * Returns array of items in specific range
     * The function is necessary when the client using lazy loads
     *
     * @param integer $from
     *   Index of the key to start from
     * @param integer $to
     *   Index of the key to stop on
     * @return mixed
     *   Array of items in specific range or null
     */
    public function getItemsInRange($from, $to)
    {
        if ($from > $to) return null;
        if ($from < 0 || $to < 0) return null;

        $keys = $this->getKeys();

        $length = count($keys);
        if ( $length < $from ) return null;
        if ( $length < $to) $to = $length - 1;

        $keysSliced = array_slice($keys, $from, ($to - $from)+1);

        return $this->getItems($keysSliced);
    }

    /**
     * Returns array of items matched with
     * regular expression that were made from $searchRequest
     *
     * @param string $searchRequest
     * @return mixed
     *  Array of items or null
     */
    public function getItemsMatchedByKey($searchRequest)
    {
        $keys = $this->getKeys();
        $pattern = "(" . $searchRequest . ")";
        $keysSorted = array();

        foreach ($keys as $key){
            $result = array();
            preg_match($pattern, $key, $result);
            if (count($result) > 0){
                $keysSorted[] = $key;
            }
        }

        if (count($keysSorted) > 0){
            $items = array();

            foreach ($keysSorted as $key){
                $items[] = $this->getItem($key);
            }
            return $items;
        } else return null;
    }

    /**
     * Returns array of items matched with
     * regular expression that were made from $searchRequest
     *
     * @param string $searchRequest
     * @return mixed
     *  Array of items or null
     */
    public function getItemsMatchedByValue($searchRequest)
    {
        $data = $this->getAllItems();
        $pattern = "(" . $searchRequest . ")";
        $itemsSorted = array();

        foreach ($data as $key => $value){
            $valueRes = array();
            preg_match($pattern, $value, $valueRes);
            if (count($valueRes) > 0){
                $itemsSorted[] = array($key => $value);
            }
        }

        if (count($itemsSorted) > 0) return $itemsSorted;
        else return null;
    }

    /**
     * Returns array of items matched with
     * regular expression that were made from $searchRequest
     *
     * @param $searchRequest
     * @return mixed
     *  Array of items or null
     */
    public function getItemsMatched($searchRequest)
    {
        $data = $this->getAllItems();
        $pattern = "(" . $searchRequest . ")";
        $itemsSorted = array();

        foreach ($data as $key => $value){
            $keyRes = array();
            $valueRes = array();
            preg_match($pattern, $key, $keyRes);
            preg_match($pattern, $value, $valueRes);
            if (count($keyRes) > 0 and count($valueRes) > 0){
                $itemsSorted[] = array($key => $value);
            }
        }

        if (count($itemsSorted) > 0) return $itemsSorted;
        else return null;
    }

    /**
     * @param PhpCacheItem $item
     * @param int|null $ttl seconds from now
     *
     * @return bool true if saved
     */
    protected function storeItemInCache(PhpCacheItem $item, $ttl)
    {
        if ($ttl === null) {
            $ttl = 0;
        } elseif ($ttl < 0) {
            return false;
        } elseif ($ttl > 86400 * 30) {
            // Any time higher than 30 days is interpreted as a unix timestamp date.
            // https://github.com/memcached/memcached/wiki/Programming#expiration
            $ttl = time() + $ttl;
        }

        $key = $this->getHierarchyKey($item->getKey());

        return $this->cache->set($key, serialize([true, $item->get(), $item->getTags(), $item->getExpirationTimestamp()]), $ttl);
    }

    /**
     * Fetch an object from the cache implementation.
     *
     * If it is a cache miss, it MUST return [false, null, [], null]
     *
     * @param string $key
     *
     * @return array with [isHit, value, tags[], expirationTimestamp]
     */
    protected function fetchObjectFromCache($key)
    {
        if (false === $result = unserialize($this->cache->get($this->getHierarchyKey($key)))) {
            return [false, null, [], null];
        }

        return $result;
    }

    /**
     * Clear all objects from cache.
     *
     * @return bool false if error
     */
    protected function clearAllObjectsFromCache()
    {
        return $this->cache->flush();
    }

    /**
     * Remove one object from cache.
     *
     * @param string $key
     *
     * @return bool
     */
    protected function clearOneObjectFromCache($key)
    {
        $this->commit();
        $path = null;
        $key  = $this->getHierarchyKey($key, $path);
        if ($path) {
            $this->cache->increment($path, 1, 0);
        }
        $this->clearHierarchyKeyCache();

        if ($this->cache->delete($key)) {
            return true;
        }

        // Return true if key not found
        return $this->cache->getResultCode() === \Memcached::RES_NOTFOUND;
    }

    /**
     * Get an array with all the values in the list named $name.
     *
     * @param string $name
     *
     * @return array
     */
    protected function getList($name)
    {
        // Implement getList() method.
    }

    /**
     * Remove the list.
     *
     * @param string $name
     *
     * @return bool
     */
    protected function removeList($name)
    {
        // Implement removeList() method.
    }

    /**
     * Add a item key on a list named $name.
     *
     * @param string $name
     * @param string $key
     */
    protected function appendListItem($name, $key)
    {
        // Implement appendListItem() method.
    }

    /**
     * Remove an item from the list.
     *
     * @param string $name
     * @param string $key
     */
    protected function removeListItem($name, $key)
    {
        // Implement removeListItem() method.
    }

    public function getDirectValue($name)
    {
        return $this->cache->get($name);
    }

    /**
     * {@inheritdoc}
     */
    public function setDirectValue($name, $value)
    {
        $this->cache->set($name, $value);
    }

    public function addItem($key, $value, $tt)
    {
        $this->cache->add($key, $value, null, $tt);
    }

    public function addServer($host, $port)
    {
        return $this->cache->addServer($host, $port);
    }

    public function getItem($key)
    {
        return [$key => $this->getDirectValue($key)];
    }
}