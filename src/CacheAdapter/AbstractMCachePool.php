<?php
/**
 * Created by PhpStorm.
 * User: return0
 * Date: 6/14/2018
 * Time: 1:13 AM
 */

namespace CacheAdapter;

use Cache\Adapter\Common\AbstractCachePool;

abstract class AbstractMCachePool extends AbstractCachePool
{
    protected abstract function init();

    public abstract function addServer($host, $port);

    /**
     * Returns all the keys stored in cache to the client
     *
     * @return array
     *  Array with all the keys
     */
    public abstract function getKeys();

    /**
     * Returns all items stored in cache server to the client
     *
     * @return array of CacheItem
     *  Array with all items from cache
     */
    public abstract function getAllItems();

    /**
     * Returns array of items in specific range
     * The function is necessary when the client using lazy loads
     *
     * @param integer $from
     *   Index of the key to start from
     * @param integer $to
     *   Index of the key to stop on
     * @return array
     *   Array of items in specific range
     */
    public abstract  function getItemsInRange($from, $to);

    /**
     * Returns array of items matched with
     * regular expression that were made from $searchRequest
     *
     * @param string $searchRequest
     * @return mixed
     *  Array of items or null
     */
    public abstract  function getItemsMatchedByKey($searchRequest);

    /**
     * Returns array of items matched with
     * regular expression that were made from $searchRequest
     *
     * @param string $searchRequest
     * @return mixed
     *  Array of items or null
     */
    public abstract function getItemsMatchedByValue($searchRequest);

    /**
     * Returns array of items matched with
     * regular expression that were made from $searchRequest
     *
     * @param $searchRequest
     * @return mixed
     *  Array of items or null
     */
    public abstract  function getItemsMatched($searchRequest);

    public abstract function getDirectValue($name);

    /**
     * {@inheritdoc}
     */
    public abstract function setDirectValue($name, $value);

    public abstract function addItem($key, $value, $tt);

    public function getItems(array $keys = [])
    {
        $items = [];
        foreach ($keys as $key) {
            $items[] = $this->getItem($key);
        }

        return $items;
    }
}